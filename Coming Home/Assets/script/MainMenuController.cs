﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour
{
    public void GoToGame()
    {
        SceneManager.LoadScene("main");
    }

    public void Setting()
    {
        SceneManager.LoadScene("setting");
    }

    public void credit()
    {
        SceneManager.LoadScene("credit");
    }
}
